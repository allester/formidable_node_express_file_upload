## File Upload Using Formidable and Express

A simple Node.js application to illustrate the use of the formidable npm package

Formidable - A Node.js module for parsing form data, expecially file uploads


### Steps to Run Application

git clone this repository to the desired location on your computer

in the root of your project, open the terminal and run `npm install`  to install all required packages

run `npm start` to run application and test it out